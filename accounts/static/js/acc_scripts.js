$(document).ready(function() {

	$(function () {
        $("#accordion").accordion({
            collapsible: true,
            clearStyle: true,
		    active: false,
            heightStyle: "content",
        });
    });

});