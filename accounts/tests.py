from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class AccountsUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/accounts/')
		self.assertEqual(response.status_code,200)

	def test_url_is_not_exist(self):
		response = Client().get('/invalid/')
		self.assertEqual(response.status_code,404)

	def test_using_accounts_template(self):
		response = Client().get('/accounts/')
		self.assertTemplateUsed(response, 'acc.html')

	def test_using_view_index_func(self):
		found = resolve('/accounts/')
		self.assertEqual(found.func, index_acc)

	def test_login_url_is_exist(self):
		response = Client().get('/accounts/login')
		self.assertEqual(response.status_code,301)

	def test_signup_url_is_exist(self):
		response = Client().get('/accounts/signup')
		self.assertEqual(response.status_code,301)

	def test_forms_validation(self):
		form_data = {'username' : 'hai', 'password1': 'haihaihai', 'password2': 'haihaihai'}
		form = UserCreationForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_POST_success_to_save_status(self):
		counting_status_object = User.objects.count()
		response = Client().post('/accounts/signup', data={'username' : 'hai', 'password1': 'haihaihai', 'password2': 'haihaihai'})
		self.assertEqual(counting_status_object, 0)
		self.assertEqual(response.status_code, 301)

	def test_model_create_new_status(self):
		new_status = User.objects.create(username= 'hai', password = 'haihaihai')
		counting_status_object = User.objects.all().count()
		self.assertEqual(counting_status_object, 1)

	def test_login_success(self):
		user = User.objects.create(username='testuser')
		user.set_password('12345')
		user.save()
		c = Client()
		logged_in = c.login(username='testuser', password='12345')

	def test_logout_url_is_exist(self):
		response = Client().get('/accounts/logout')
		self.assertEqual(response.status_code, 301)




class AccountsFuncTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(AccountsFuncTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(AccountsFuncTest, self).tearDown()

	def test_landing_page_element_greet(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/accounts')
		time.sleep(3)

		greet = self.selenium.find_element_by_id('accordion')
		self.assertEqual(greet.find_element_by_id('login').text, 'LOG IN')
		self.assertEqual(greet.find_element_by_id('signup').text, 'SIGN UP')

	def test_signup(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/accounts')
		time.sleep(3)

		acc = self.selenium.find_element_by_id('accordion')
		acc.find_element_by_id('signup').click()
		time.sleep(3)

		username = self.selenium.find_element_by_id('id_username')
		password1 = self.selenium.find_element_by_id('id_password1')
		password2 = self.selenium.find_element_by_id('id_password2')
		sub = self.selenium.find_element_by_id('signup-but')

		username.send_keys("atest")
		password1.send_keys("cek321cek")
		password2.send_keys("cek321cek")

		sub.click();
		time.sleep(3)

		self.assertIn("Welcome atest!", self.selenium.page_source)
		time.sleep(3)


	def test_signin(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/accounts')
		time.sleep(3)

		acc = self.selenium.find_element_by_id('accordion')
		acc.find_element_by_id('login').click()
		time.sleep(3)

		username = self.selenium.find_element_by_id('username_login')
		password = self.selenium.find_element_by_id('password_login')
		log = self.selenium.find_element_by_id('login-but')

		username.send_keys("ilmarohma")
		password.send_keys("13122000ilma")

		log.click();
		time.sleep(3)

		self.assertIn("Welcome ilmarohma!", self.selenium.page_source)
		time.sleep(3)



