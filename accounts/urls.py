from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'accounts'
urlpatterns = [
    path('', index_acc, name='index'),
    path('login/', auth_login, name='login'),
    path('signup/', signup, name='signup'),
    path('logout/', logoutView, name='logout')
]
