from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

def index_acc(request):
	form = UserCreationForm()
	return render(request, 'acc.html', {'form' : form})

def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			return redirect('/')
	else:
		form = UserCreationForm()
	return render(request, 'acc.html', {'form' : form})

def auth_login(request):
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username , password=password)
		if user is not None:
			login(request, user)
			return redirect('/')
	return redirect('/accounts/')

@login_required
def logoutView(request):
    if request.method == "GET":
        if request.user.is_authenticated:
            logout(request)
    return redirect('/')

