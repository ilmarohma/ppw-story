$(document).ready(function () {
    $('#search-button').click(function () {
        var key = $('#keyword-box').val();
        $(function () {
            $.ajax({
                url: "/books/JSON/" + key,
                success: function (result) {
                    $('#content-table').empty();
                    $.each(result.items, function (i, item) {
                        $('<tr>').append(
                            $('<th scope="row">').text((i+1)),
                            $('<td>').append('<img style="max-height: 150px;max-width: 150px" src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
                            $('<td>').text(item.volumeInfo.title),
                            $('<td>').text(item.volumeInfo.authors),
                            $('<td>').text(item.volumeInfo.publisher),
                            $('<td>').append('<a href="' + item.volumeInfo.previewLink + '" target="_blank">PREVIEW</a>'),
                        ).appendTo('#content-table');
                    });
                }
            });
        });
    });
    $('#keyword-box').val('web programming');
    $('#search-button').click();
});
