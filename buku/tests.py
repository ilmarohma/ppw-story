from django.test import TestCase, Client
from django.urls import resolve
from .views import *


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class BukuUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code,200)

	def test_url_is_not_exist(self):
		response = Client().get('/invalid/')
		self.assertEqual(response.status_code,404)

	def test_using_books_template(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'index_buku.html')

	def test_using_view_index_func(self):
		found = resolve('/books/')
		self.assertEqual(found.func, index_buku)

	def test_JSON_page_url_is_exist(self):
		response = Client().get('/books/JSON/test/')
		self.assertEqual(response.status_code, 200)

	def test_JSON_using_readJSON_func(self):
		foundjson = resolve('/books/JSON/test/')
		self.assertEqual(foundjson.func, getData)

class BukuFuncTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(BukuFuncTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(BukuFuncTest, self).tearDown()

	def test_json_has_read(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/books/')
		time.sleep(3)

		keyword = selenium.find_element_by_id('keyword-box')
		search = selenium.find_element_by_id('search-button')

		message = 'web programming'
		keyword.clear()
		keyword.send_keys(message)
		search.click()
		time.sleep(3)
		self.assertIn("1", selenium.page_source)
