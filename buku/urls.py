from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'buku'
urlpatterns = [
    path('', index_buku, name='index'),
    path('JSON/<str:keyword>/', getData, name='data')
]
