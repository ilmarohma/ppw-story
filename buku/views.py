from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests

response = {}
def index_buku(request):
	return render(request, 'index_buku.html')

def getData(request, keyword):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
	json_data = json.loads(requests.get(url).text)
	return JsonResponse(json_data)
