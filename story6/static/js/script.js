$(document).ready(function() {


	$('#day-theme').click(function(){
		$('#body').css('background-color', 'rgb(252,252,225)'); 
		$('#greet').css('background-color', 'rgb(124,217,206)');
		$('#greet').css('color', 'black');  
		$('.form-container').css('color', 'black'); 
		$('hr').css('color', 'rgb(124,217,206)');
		$('#prof').css('color', 'black');
		$('#photo').attr('src', 'static/img/my_photo1.png')
	});
	$('#night-theme').click(function(){
		$('#body').css('background-color', 'black'); 
		$('#greet').css('background-color', 'blue');
		$('#greet').css('color', 'white');  
		$('.form-container').css('color', 'white'); 
		$('hr').css('color', 'blue');
		$('#prof').css('color', 'white');
		$('#photo').attr('src', 'static/img/my_photo.png')
	});

	$(function () {
        $("#accordion").accordion({
            collapsible: true,
            clearStyle: true,
		    active: false,
            heightStyle: "content",
        });
    });

});